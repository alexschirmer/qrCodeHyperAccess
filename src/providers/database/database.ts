import { AngularFireDatabase } from "angularfire2/database";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {
  constructor(public database: AngularFireDatabase) {}

  onCheckDatabase(value: string) {
    return this.database.list("/tempPassword", ref =>
      ref.orderByChild("password").equalTo("9999")
    );
  }
}
