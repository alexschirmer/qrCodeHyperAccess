import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VisitAccessPage } from './visit-access';

@NgModule({
  declarations: [
    VisitAccessPage,
  ],
  imports: [
    IonicPageModule.forChild(VisitAccessPage),
  ],
})
export class VisitAccessPageModule {}
