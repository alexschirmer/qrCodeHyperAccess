import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  AlertController,
  LoadingController
} from "ionic-angular";
import {
  FormGroup,
  FormBuilder,
  Validators
} from "../../../node_modules/@angular/forms";
import { DatabaseProvider } from "../../providers/database/database";
import { AngularFireDatabase } from "../../../node_modules/angularfire2/database";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/first";
import * as firebase from "firebase";

@IonicPage()
@Component({
  selector: "page-visit-access",
  templateUrl: "visit-access.html"
})
export class VisitAccessPage {
  accessForm: FormGroup;
  door: Observable<any>;
  item: any;

  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public provider: DatabaseProvider,
    public database: AngularFireDatabase,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController
  ) {
    this.door = database.object("command").valueChanges();

    this.accessForm = this.formBuilder.group({
      codigo: [
        "",
        [Validators.required, Validators.minLength(4), Validators.maxLength(4)]
      ]
    });
  }

  onSubmit() {
    let lista = this.database.list("/tempPassword", ref =>
      ref.orderByChild("password").equalTo(this.accessForm.value.codigo)
    );

    lista.snapshotChanges().subscribe(item => {
      if (item.length == 0) {
        this.showAlert("Código Incorreto");
      } else {
        this.item = JSON.stringify(item);
        this.abrirPorta();
      }
    });
  }

  abrirPorta() {
    this.attCheck(true);
    this.checkAck();
  }

  private attCheck(flag: boolean) {
    const itemRef = this.database.object("command");
    itemRef.update({ open: flag });
  }

  checkAck() {
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();

    setTimeout(() => {
      this.door.first().subscribe(value => {
        if (value.opened) {
          this.showAlertAck("Sucesso", "A porta foi aberta.");
          loader.dismiss();
          this.attCheck(false);
          const itemRef = this.database.object("command");
          itemRef.update({ opened: false });
          this.CreateLog();
          return;
        } else {
          this.showAlertAck("Erro", "A porta não foi aberta.");
          loader.dismiss();
          this.attCheck(false);
        }
      });
    }, 3000);
  }

  private CreateLog() {
    const time = firebase.database.ServerValue.TIMESTAMP;

    let item = JSON.parse(this.item);

    this.database.list("logs/" + item[0].payload.id).push({
      timestamp: time,
      type: "Visitante: " + item[0].payload.name
    });
  }

  private showAlert(mensagem: string) {
    let alert = this.alertCtrl.create({
      title: mensagem,
      buttons: ["OK"]
    });
    alert.present();
  }

  private showAlertAck(title: string, message: string) {
    const confirm = this.alertCtrl.create({
      title: title,
      message: message,
      buttons: ["Ok"]
    });
    confirm.present();
  }
}
