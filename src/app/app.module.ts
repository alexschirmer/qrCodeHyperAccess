import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";

import { MyApp } from "./app.component";

import { AngularFireModule, FirebaseAppConfig } from "angularfire2";
import {
  AngularFireDatabaseModule,
  AngularFireDatabase
} from "angularfire2/database";
import { AngularFireAuthModule } from "angularfire2/auth";
import { DatabaseProvider } from "../providers/database/database";
import firebase from "../../node_modules/firebase";

const firebaseConfig: FirebaseAppConfig = {
  apiKey: "AIzaSyCvilNLOlBTVH43XPWIg5AC1vgBL9ilrqE",
  authDomain: "hyperacess.firebaseapp.com",
  databaseURL: "https://hyperacess.firebaseio.com",
  projectId: "hyperacess",
  storageBucket: "hyperacess.appspot.com",
  messagingSenderId: "570536511385"
};

firebase.initializeApp(firebaseConfig);

@NgModule({
  declarations: [MyApp],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    DatabaseProvider,
    AngularFireDatabase
  ]
})
export class AppModule {}
